# Authors: Daria Alexander, Ibrahim Amellal

# Part 2 : Words and POS Tags statistics

import sys

lexicons = []
lexicons_restricted =  []

# First task : Print the number of occurences

def count(filename):
    lexicon = {}
    lexicon["<NUMBER>"] = 0
    sentence_counter = 0
    token_counter = 0
    file = open(filename,"r",encoding="utf-8")
    for line in file:
        sentence_counter += 1
        for token in line.split():
            token_counter += 1
            i = token.rfind("/")
            word = token[:i]
            tag = token[i+1:]
            if word not in lexicon:
                lexicon[word] = 1
            else:
                lexicon[word] += 1
    print("File : " + filename)
    print("Number of tokens : " + str(token_counter))
    print("Number of sentences : " + str(sentence_counter))
    lexicons.append(lexicon)
    file.close()

count("dumas_train")
count("dumas_test")

# Second task : Replacements

def replace(file1,file2):
    file = open(file1,"r",encoding="utf-8")
    output = open(file2,"w",encoding="utf-8")
    for line in file:
        sentence = ""
        for token in line.split():
            if sentence != "":
                sentence += " "

            i = token.rfind("/")
            word = token[:i]
            tag = token[i+1:]
            if tag == "CD":
                sentence += "<NUMBER>/CD"
            elif tag == "NNP":
                sentence += "<PROPER_NOUN>" + "/" + tag
            elif tag == "NNPS":
                sentence += "<PROPER_NOUN_PLURAL>" + "/" + tag
            else:
                sentence += word+ "/" + tag
        output.write(sentence + "\n")

    file.close()
    output.close()

replace("dumas_train","train")
replace("dumas_test","test")

for lex in lexicons:
    lexicon_restricted = {}
    for word in sorted(lex.keys()):
        if lex[word] > 3:
            lexicon_restricted[word] = lex[word]
    lexicons_restricted.append(lexicon_restricted)


print("Size of restricted lexicon : " + str(len(lexicons_restricted[0])) + " & " + str(len(lexicons_restricted[1])))

def replace2(file1,file2):
    file = open(file1,"r",encoding="utf-8")
    output = open(file2,"w",encoding="utf-8")
    for line in file:
        sentence = ""
        for token in line.split():
            if sentence != "":
                sentence += " "

            i = token.rfind("/")
            word = token[:i]
            tag = token[i+1:]
            if word not in lexicon_restricted:
                sentence +=  "<UNK>/" + tag
            else:
                sentence += word+ "/" + tag
        output.write(sentence + "\n")

    file.close()
    output.close()

replace2("dumas_train","train")
replace2("dumas_test","test")

# Third task: 10 major occurences
print("10 majors occurences : ")

def top(index,number):
    i = 0
    for word in sorted(lexicons_restricted[index],key=lexicons_restricted[index].get, reverse=True):
        if i == number:
            break
        print(word)
        i += 1

print("Top 10 in train : ")

top(0,10)

print("Top 10 in test : ")
top(1,10)

# Fourth task: occurencies of words

def decreaseListing(lexicons_restricted):
    for tag in sorted(lexicons_restricted[0],key=lexicons_restricted[0].get, reverse=True):
        print(tag + " : " + str(lexicons_restricted[0][tag]))

print("Words by their frequencies : ")
decreaseListing(lexicons_restricted)
