# @Author : Daria Alexander



import numpy as np
import hmms
import json
import time


t0 = time.time()


# 1. Couing form train.gz & HMM building

file = open("train","r",encoding="utf8")

tags = [] # We list all tags without repetitions, total 44.
tag_count = 0 # Number of tags, max 44
lexicon = {} #
tokens_tags =[] # We append all tags one after one
w = 0 # Number of words

# collect all tags distinctly
def collectTags():

    for line in file: # For each line in file
        tokens_line = [] # initialize an array to stock all tokens
        for token in line.split(): # For all tokens in line
            tokens_line.append(token) # Add the token
            i = token.rfind("/") # Split the token
            word = token[:i]
            tag = token[i+1:]
            w += 1 # Increment
            if tag not in tags : # For all tags not in already known tags
                tags.append(tag)
                tag_count += 1
        tokens_tags.append(tokens_line)
collectTags()

tags_next = {} #
file.close()


jsonA = {}

# We initialize the json for Q-> Q
def initQQ():
    for i in range(0,tag_count):
        jsonA[tags[i]] = {}
        for j in range(0,tag_count):
            jsonA[tags[i]][tags[j]] = 0

initQQ()

# We initialize the json Q-> W
jsonB = {}

def initQW():
    for i in range(0,tag_count):
        jsonB[tags[i]] = {}
        jsonB[tags[i]]['<UNK>'] = 0
        jsonB[tags[i]]['<NUMBER>'] = 0
        jsonB[tags[i]]['<PROPER_NOUN>'] = 0
        jsonB[tags[i]]['<PROPER_NOUN_PLURAL>'] = 0


initQW()

def counting():
    chTab = {}
    i = 0
    for tagToken in tags:
        ch = 0
        for i in range(0, len(tokens_tags)):
            for j in range(0, len(tokens_tags[i])):
                l = tokens_tags[i][j].rfind("/")
                word = tokens_tags[i][j][:l]
                #print(word)
                #word = word.replace('\n','')
                tag = tokens_tags[i][j][l+1:]
                if tagToken == tag:
                    if j+1<len(tokens_tags[i]):
                        ch += 1
                        nextToken = tokens_tags[i][j+1]
                        m = nextToken.rfind("/")
                        nextWord = nextToken[:m]
                        nextTag = nextToken[m+1:]
                        #nextWord = nextWord.replace('\n','')
                        jsonA[tag][nextTag] += 1

                        if nextWord in jsonB[tag]:
                            jsonB[tag][nextWord] += 1
                        else:
                            jsonB[tag][nextWord] = 1
        chTab[tagToken] = ch

counting()


chMatrix = {}

# Initialization of matrix A, the matrix Q-> Q
P = [[0 for x in range(0,len(tags))] for y in range(0,len(tags))]
jsonMatrixA = {}
jsonMatrixB = {}

def createA():
    for tagToken in tags:
        abs = 0
        chMatrix[tagToken] = chTab[tagToken]/tag_count
        b = chMatrix[tagToken] + tag_count
        # Laplace smoothing
        for j in tags:
            ord = 0
            a = jsonA[tagToken][j] + 1
            pij = a/b
            #print(pij)
            P[abs][ord] = pij
            #print(pij)
            ord += 1
        abs += 1

createA()

A = P
words = {}
#print("A finished")


wordsTab = [] # List of words

file = open("lexicon","r",encoding="utf8")
for line in file:
        wordsTab.append(line)
file.close()

i = 0
def replacePunct():
    for word in wordsTab:
        word = word.replace('\n', '')
        word = word.replace('.', '')
        word = word.replace(',', '')
        wordsTab[i] = word
        i += 1

replacePunct()

B = [[0 for x in range(0,len(wordsTab))] for y in range(0,len(tags))]
abs = 0

def createB():
    for tagToken in tags:
        ord = 0
        chMatrix[tagToken] = chTab[tagToken] / w
        b = chMatrix[tagToken] + w
        for word in wordsTab:

            if word != "":
                if jsonB[tagToken].get(word):
                    a = jsonB[tagToken][word] + 1
                    #print(a)
                else:
                    a = 1
                bij = a / b
                B[abs][ord] = bij
                #print(bij)
                ord += 1

        abs += 1

createB()

# Normalization
m1 = [[0 for x in range(0,len(tags))] for y in range(0,len(tags))]
m2 = [[0 for x in range(0,len(tags))] for y in range(0,len(wordsTab))]

listOfAcc1 = []
listOfAcc2 = []

def normalizaiton():
    for list in A:
        i = 0
        acc = 0
        for number in list:
            acc += number
            #print(number)
        listOfAcc1.append(acc)
        for number in list:
            j = 0
            if acc > 0:
                m1[i][j] = number/acc
            else:
                m1[i][j] = 0
            j += 1
        i += 1

    for list in B:
        i = 0
        acc = 0

        for number in list:
            acc += number

        """print("acc : ")
        print(acc)"""
        for number in list:
            j = 0
            if acc > 0:
                """print("n : ")
                print(number)
                print("acc")
                print(acc)
                print("quot")
                print(number/acc)"""
                m2[i][j] = number/acc
            else:
                m2[i][j] = 0

            j += 1
        i += 1

normalization()

print("Finished")

# Save the matrix A
with open('matrix.txt', 'w') as outfile:
    json.dump(chMatrix, outfile)

# Save the JSON data

with open('jsonA.txt', 'w') as outfile:
    json.dump(jsonA, outfile)
with open('jsonB.txt', 'w') as outfile2:
    json.dump(jsonB, outfile2)

with open('jsonMatrixA.txt', 'w') as outfile:
    json.dump(jsonMatrixA, outfile)
with open('jsonMatrixB.txt', 'w') as outfile2:
    json.dump(jsonMatrixB, outfile2)

"""matrixQ = np.array(A)
matrixW = np.array(B)
np.save('matrixQ',matrixQ)
np.save('matrixW',matrixW)


#matrixA = np.load('matrixQ.npy')
#matrixB = np.load('matrixW.npy')

proba1 = np.array(m1)
proba2 = np.array(m2)


np.savetxt('proba1',proba1)
np.savetxt('proba2',proba2)

np.save('proba1',proba1)
np.save('proba2',proba2)

p1 = np.load('proba1.npy')
p2 = np.load('proba2.npy')

print(m2)"""


# 2. Report the tagging test error rate
print("Time elapsed : ")
print(time.time() - t0)


#dhmm = hmms.DtHMM(p1,p2,p1[0])
#dhmm.save_params("dthmm")



s_seq = ['WDT','NN','IN','NN','VBZ','VBC','CC','VBZ','PRP','VB','JJ','NN']

test_s_seq, test_e_seq = dhmm.generate(12)

(log_prob, s_seq) =  dhmm.viterbi(test_e_seq)

# 2. Tagging test error rate

# 3.
