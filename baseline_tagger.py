# Authors: Daria Alexander, Ibrahim Amellal
import re
#import lexicon.py

def percent_of_error(file_name, main_tag_dict):
    errors     = 0
    errors_RB  = 0
    errors_VBN = 0
    num_words  = 0
    num_RB     = 0
    num_VBN    = 0

    dict_RB  = {}
    dict_VBN = {}
    with open(file_name, "r", encoding="utf-8") as line_list: # cycle by lines in our file
        for line in line_list:
            for token in line.split(): # cycle by words in current line
                num_words += 1
                i = token.rfind("/")
                word = token[:i]
                tag = token[i + 1:]
                if word in main_tag_dict.keys():
                    my_tag = main_tag_dict[word]
                    if tag != my_tag:
                        errors += 1
                    if tag == 'RB':
                        num_RB += 1
                        if tag != my_tag:
                            errors_RB += 1
                            dict_RB.setdefault(my_tag, 0)
                            dict_RB[my_tag] += 1
                    elif tag == 'VBN':
                        num_VBN += 1
                        if tag != my_tag:
                            errors_VBN += 1
                            dict_VBN.setdefault(my_tag, 0)
                            dict_VBN[my_tag] += 1
                else:
                    errors += 1
    print('\nerror', errors, 'words', num_words, 'percent of error', errors*100/num_words)
    print('\nerror RB', errors_RB, 'words RB', num_RB, 'percent of error RB', errors_RB*100/num_RB)
    for key in dict_RB:
        print('mistake tag', key, 'errors', dict_RB[key])
    print('\nerror VBN', errors_VBN, 'words VBN', num_VBN, 'percent of error VBN', errors_VBN*100/num_VBN)
    for key in dict_VBN:
        print('mistake tag', key, 'errors', dict_VBN[key])



def determine_main_tag(words_dict):
    main_tag_dict = {}
    for key in words_dict:
        stat_dict = words_dict[key]
        v = list(stat_dict.values()) # put the dictionary's values into a list
        k = list(stat_dict.keys())   # put the dictionary's keys into a list
        main_tag_dict[key] = k[v.index(max(v))] # it is a key of max value
    return main_tag_dict

def getstat(file_name):
    word_dict = {} # create a dictionary for statistics per words
    with open(file_name, "r", encoding="utf-8") as line_list:
        for line in line_list:
            for token in line.split():
                i = token.rfind("/")
                word = token[:i]
                tag = token[i + 1:]
                if word not in word_dict.keys():
                    word_dict[word] = {} # if the word is not in the dictionary, we add it
                stat_dict = word_dict[word]
                stat_dict.setdefault(tag, 0)
                stat_dict[tag] += 1 # increase the number of words that were found

    return word_dict


def readfile(input_file_name, output_file_name):
    output_file = open(output_file_name, 'w') # open the file for the output
    exception_list = ['.', ',', "''", "''",'!', '?', "'s", ';', ':', "'", "`"] # before those words we should not add space
    previous_exception_list = ["``", ''] # after those words we should not add space
    with open(input_file_name, "r", encoding="utf-8") as line_list: # opening a file
        for line in line_list:
            previous_word = ''
            for token in line.split(): # cycle by words in the current line
                i = token.rfind("/")
                word = token[:i]
                # if the word is not in the list of exceptions we add space
                if word not in exception_list and previous_word not in previous_exception_list:
                    output_file.write(' ')
                output_file.write(word)
                previous_word = word # we remember the previous word
                #print(word) # debugging print
            output_file.write('\n') # add a character of the end of the line
    output_file.close()

def write_file(input_file_name, output_file_name, main_tag_dict):
    output_file = open(output_file_name, 'w') # open the file for the output
    exception_list = ['.', ',', "''", "''",'!', '?', "'s", ';', ':', "'", "`"] # we should not add space before those words
    previous_exception_list = ["``", ''] # We should not add space after those words
    with open(input_file_name, "r", encoding="utf-8") as line_list:
        for line in line_list:
            line = line.rstrip()
            previous_word = ''
            #line_split = re.findall(r"[\w']+|[.,!?:;''```]", line)
            line_split = re.split("(\W+)", line)
            for word in line_split:
                # if the word is not in the list of exceptions, we add a space
                word = ''.join(word.split())
                if word != ' ' and len(word) > 0:
                    if word not in exception_list and previous_word not in previous_exception_list:
                        output_file.write(' ')
                    output_file.write(word)
                    if word in main_tag_dict.keys():
                        cur_tag = '/' + main_tag_dict[word]
                    else:
                        cur_tag = '/NONE'
                    output_file.write(cur_tag)
                    previous_word = word # remember the previous word
                    #print(word) # debugging print
            output_file.write('\n') # add an end of line character
    output_file.close()

if __name__ == '__main__' :
    #readfile('dumas_test', 'output_test_file')
    #readfile('dumas_train', 'output_train_file')
    words_test_dict = getstat('dumas_test')
    words_dict = getstat('dumas_train')

    #lexicon.count('dumas_train')

    # What is the most likely tag for the word start?
    print('For word "start"')
    frequency_start = words_dict['start']
    for key in frequency_start:
        print('for', key, 'count', frequency_start[key])

    # What word has the biggest number of tags? How many times does each of those tags appear for this word?
    max_tags_word = ''
    max_tags = 0
    for key in words_dict:
        stat_dict = words_dict[key]
        count_tags = len(stat_dict)
        if count_tags > max_tags:
            max_tags = count_tags
            max_tags_word = key

    print('\nmax tag has', max_tags_word)
    stat_dict = words_dict[max_tags_word]
    for key in stat_dict:
        print('for', key, 'count', stat_dict[key])

    # we should determine the main tag
    main_tag_dict = determine_main_tag(words_dict)

    #Count the percentage of error of your baseline tagger.
    percent_of_error('dumas_test', main_tag_dict)
    write_file('output_test_file', 'test_file_with_tag', main_tag_dict)

   # print(words_dict)

    print('top 10 words in train')
    print(',',words_dict[','])
    print('the',words_dict['the'])
    print('.',words_dict['.'])
    print('I',words_dict['I'])
    print('``',words_dict['``'])
    print('would',words_dict['would'])
    print('to',words_dict['to'])
    print('of',words_dict['of'])
    print('and',words_dict['and'])
    print('a',words_dict['a'])
